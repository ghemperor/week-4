import 'package:flutter/material.dart';
class Box extends StatelessWidget {
  Box({required this.title, required this.action, required this.child});
  final Widget title;
  final Widget action;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              title,
              action
            ],
          ),
        ],
      ),
    );
  }
}
