import 'package:flutter/material.dart';

class SliverHomeAppBar extends StatelessWidget {
  SliverHomeAppBar({required this.onMenuIconPressed});
  final VoidCallback onMenuIconPressed;


  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      title: Text("TIKI.VN"),
      leading: IconButton(icon: Icon(Icons.menu), onPressed: onMenuIconPressed,),
      actions: <Widget>[
        Stack(
          children: <Widget>[
            IconButton(icon: Icon(Icons.shopping_cart), onPressed: () {},),
            Positioned(
              top: 8.0,
              right: 8.0,
              child: Container(
                width: 16.0,
                height: 16.0,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.deepOrangeAccent
                ),
                child: Text("14", style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold)),
              ),
            ),
          ],
        )
      ],
      expandedHeight: 120.0,
      pinned: true,
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(24.0), // luu y
        child: Container(
            padding: EdgeInsets.all(16.0),
            child: Container(
              height: 48.0,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(4.0),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.3), blurRadius: 5.0, offset: Offset(0.0, 0.0) //BoxShadow with Opacity
                    )
                  ]
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[

                  Expanded(
                    child: Text("   Tìm sản phẩm, danh mục hay thương hiệu mong muốn ...",
                      style: TextStyle(
                          color: Colors.grey[600], fontSize: 16.0),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  IconButton(icon: Icon(Icons.search, color: Colors.grey[600],), onPressed: () {},),
                ],
              ),
            )
        ),
      ),
    );
  }
}
