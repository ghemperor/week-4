import 'package:flutter/material.dart';
import 'package:week4/home_screen.dart';

void main() {
  runApp(
      MaterialApp(
        title: "Tiki",
        debugShowCheckedModeBanner: false,
        home: HomeScreen(),
      ),
  );
}
